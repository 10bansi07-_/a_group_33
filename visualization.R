#To call the Tidyverse library
library(tidyverse)
#Loading Dataset
d <- read.csv("Dataset_Group33.csv")
#data cleaning (removing null vales from dependent variable)
cleaned_data<-d[!is.na(d$Suicide.rate..deaths.per.100.000.individuals.),]

pdf("visualization.pdf")

x<-cleaned_data$Depressive.disorder.rates..number.suffering.per.100.000.
y<-cleaned_data$Suicide.rate..deaths.per.100.000.individuals.
#plotting Scatter plot
plot(x
     ,y
     ,xlab="Depression cases"
     ,ylab="Suicide  cases"
     ,main="Global suicide rate vs depression rate:2010 to 2017"
     ,pch =19
     ,cex =0.2
     ,frame = T
)
model<- lm(y~x, data=cleaned_data)
abline(model, col="blue")

y<- cleaned_data$Suicide.rate..deaths.per.100.000.individuals.
#plotting histogram
h <- hist(y 
          ,12
          , main = "Histogram "
          , xlab = "Suicide cases"
          , ylab = "Frequency"
          , col  = "azure"
          
)
dmin<-min(cleaned_data$Suicide.rate..deaths.per.100.000.individuals.)
dmax<-max(cleaned_data$Suicide.rate..deaths.per.100.000.individuals.)
mn  <- mean(y)
stdD<- sd(y)
x <- seq(dmin,dmax,)
y1 <- dnorm(x, mean=mn, sd=stdD)
y1 <- y1 * diff(h$mids[1:2]) * length(y);
lines(x, y1, col="blue")
dev.off()

